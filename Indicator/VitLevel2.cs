#region License and Terms
/*
The MIT License (MIT)

Copyright (c) 2013-2014 Vitalij <vitalij@gmx.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#endregion

#region Using declarations

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Gui.Design;

#endregion Using declarations

namespace NinjaTrader.Indicator {

	[Description("Vitalij's Level II indicator for NinjaTrader 7" + "\r\n" + "http://ninjatrader.bitbucket.org/")]
	public class VitLevel2 : Indicator {

		#region Variables

		Dictionary<double, long> rowsAsk = new Dictionary<double, long>();
		Dictionary<double, long> rowsBid = new Dictionary<double, long>();

		int maxBarWidth = 100;
		int maxBarHeight = 50;
		int minBarHeight = 3;

		int marginLeft = 0;
		int marginRight = 0;

		int rowLimit = int.MaxValue;

		HorizontalAlignment alignment = HorizontalAlignment.Right;

		string error = "No market depth data available!";
		Font textFont = new Font("Arial", 8, FontStyle.Regular);

		StringFormat sfLeftTop = new StringFormat();
		StringFormat sfLeftCenter = new StringFormat();
		StringFormat sfLeftBottom = new StringFormat();
		StringFormat sfRightTop = new StringFormat();
		StringFormat sfRightCenter = new StringFormat();
		StringFormat sfRightBottom = new StringFormat();

		int opacityAskBars = 50;
		int opacityBidBars = 50;

		Color colorAskBars = Color.Red;
		Color colorBidBars = Color.LimeGreen;
		Color colorAskText = Color.Black;
		Color colorBidText = Color.Black;
		Color colorAskTextSum = Color.Black;
		Color colorBidTextSum = Color.Black;

		Brush brushAskBar;
		Brush brushBidBar;
		Brush brushAskText;
		Brush brushBidText;
		Brush brushAskTextSum;
		Brush brushBidTextSum;

		Pen penOrderBookMedian = new Pen(Color.Silver);

		#endregion Variables

		#region Methods

		protected override void Initialize() {
			CalculateOnBarClose = true;
			Overlay = true;
			AutoScale = false;

			Name = "Level II";

			sfLeftTop.Alignment = StringAlignment.Far;
			sfLeftTop.LineAlignment = StringAlignment.Far;
			sfLeftCenter.Alignment = StringAlignment.Far;
			sfLeftCenter.LineAlignment = StringAlignment.Center;
			sfLeftBottom.Alignment = StringAlignment.Far;
			sfLeftBottom.LineAlignment = StringAlignment.Near;
			sfRightTop.Alignment = StringAlignment.Near;
			sfRightTop.LineAlignment = StringAlignment.Far;
			sfRightCenter.Alignment = StringAlignment.Near;
			sfRightCenter.LineAlignment = StringAlignment.Center;
			sfRightBottom.Alignment = StringAlignment.Near;
			sfRightBottom.LineAlignment = StringAlignment.Near;

			brushAskText = new SolidBrush(colorAskText);
			brushBidText = new SolidBrush(colorBidText);
			brushAskTextSum = new SolidBrush(colorAskTextSum);
			brushBidTextSum = new SolidBrush(colorBidTextSum);
			brushAskBar = new SolidBrush(Color.FromArgb(opacityAskBars * 255 / 100, colorAskBars));
			brushBidBar = new SolidBrush(Color.FromArgb(opacityAskBars * 255 / 100, colorBidBars));

			penOrderBookMedian.DashStyle = DashStyle.Dot;
		}

		protected override void OnTermination() {
			sfLeftTop.Dispose();
			sfLeftCenter.Dispose();
			sfLeftBottom.Dispose();
			sfRightTop.Dispose();
			sfRightCenter.Dispose();
			sfRightBottom.Dispose();

			brushAskBar.Dispose();
			brushBidBar.Dispose();
			brushAskText.Dispose();
			brushBidText.Dispose();
			brushAskTextSum.Dispose();
			brushBidTextSum.Dispose();
		}

		protected override void OnStartUp() {
			//ChartControl.BarMarginRight = marginLeft + maxBarWidth + marginRight;
		}

		protected override void OnMarketDepth(MarketDepthEventArgs ev) {
			if (ev.MarketDataType == MarketDataType.Ask) {
				SetMarketDepthVolume(ev, rowsAsk);
			} else if (ev.MarketDataType == MarketDataType.Bid) {
				SetMarketDepthVolume(ev, rowsBid);
			}
		}

		protected void SetMarketDepthVolume(MarketDepthEventArgs ev, Dictionary<double, long> rows) {
			if (ev.Price == 0) {
				rowsAsk.Clear();
				rowsBid.Clear();
			}

			if (ev.Operation == Operation.Remove) {
				rows.Remove(ev.Price);
			} else /* if (ev.Operation == Operation.Insert || ev.Operation == Operation.Update) */ {
				rows[ev.Price] = ev.Volume;
			}

			ChartControl.ChartPanel.Invalidate();
		}

		public override void Plot(Graphics graphics, Rectangle bounds, double min, double max) {
			// FIXME: find a reliable solution
			// if right-most bar is not a real-time updating bar, then do not display Level II histogram
			//if (ChartControl.GetXByBarIdx(Bars, Bars.Count - ((CalculateOnBarClose) ? 1 : 2)) > bounds.Right - ChartControl.BarMarginRight - ChartControl.Margin.Right) {
			//	return;
			//}

			// display error message
			if (!DisplayTime() || rowsAsk.Count < 1 || rowsBid.Count < 1) {
				graphics.DrawString(error, ChartControl.Font, new SolidBrush(ChartControl.AxisColor), bounds.Right, bounds.Bottom, sfLeftTop);
				return;
			}

			// limit rows
			var rowsA = rowsAsk.OrderBy(kv => kv.Key).Take(rowLimit);
			var rowsB = rowsBid.OrderByDescending(kv => kv.Key).Take(rowLimit);

			// sum up volume
			long maxAskVolume = rowsA.Max(v => v.Value);
			long maxBidVolume = rowsB.Max(v => v.Value);

			// calc scale/dimensions/size
			float barWidthScale = (float)maxBarWidth / Math.Max(maxAskVolume, maxBidVolume);
			int[] y = GetYsByPrice(TickSize);
			bool isCramped = (textFont.GetHeight(graphics) > y[2]);
			int halfHeight = y[3] / 2;

			// TODO: clean this mess up
			int barLeft = bounds.Right - maxBarWidth - marginRight;
			int txtLeft = barLeft;
			StringFormat txtAlignment = sfRightCenter;
			StringFormat sumAskAlignment = sfRightTop;
			StringFormat sumBidAlignment = sfRightBottom;
			if (alignment == HorizontalAlignment.Right) {
				sumAskAlignment = sfLeftTop;
				sumBidAlignment = sfLeftBottom;
			}
			int crampedLeft = 0;
			StringFormat crampedAlignment = sfLeftCenter;

			// draw ask bars
			foreach (KeyValuePair<double, long> row in rowsA) {
				int barWidth = (int)Math.Round(row.Value * barWidthScale);
				y = GetYsByPrice(row.Key);

				// TODO: clean this mess up
				if (alignment == HorizontalAlignment.Right) {
					barLeft = bounds.Right - barWidth - marginRight;
					txtLeft = bounds.Right - marginRight;
					txtAlignment = sfLeftCenter;

					crampedLeft = barLeft;
					crampedAlignment = sfRightCenter;
				} else {
					crampedLeft = barLeft + barWidth;
				}

				graphics.FillRectangle(brushAskBar, barLeft, y[0] - halfHeight, barWidth, y[3]);
				if (!isCramped) {
					graphics.DrawString(row.Value.ToString(), textFont, brushAskText, txtLeft, y[0], txtAlignment);
				} else if (row.Value == maxAskVolume) {
					graphics.DrawString(row.Value.ToString(), textFont, brushAskText, crampedLeft, y[0], crampedAlignment);
				}
			}

			// draw bid bars
			foreach (KeyValuePair<double, long> row in rowsB) {
				int barWidth = (int)Math.Round(row.Value * barWidthScale);
				y = GetYsByPrice(row.Key);

				// TODO: clean this mess up
				if (alignment == HorizontalAlignment.Right) {
					barLeft = bounds.Right - barWidth - marginRight;
					txtLeft = bounds.Right - marginRight;
					txtAlignment = sfLeftCenter;

					crampedLeft = barLeft;
					crampedAlignment = sfRightCenter;
				} else {
					crampedLeft = barLeft + barWidth;
				}

				graphics.FillRectangle(brushBidBar, barLeft, y[0] - halfHeight, barWidth, y[3]);
				if (!isCramped) {
					graphics.DrawString(row.Value.ToString(), textFont, brushBidText, txtLeft, y[0], txtAlignment);
				} else if (row.Value == maxBidVolume) {
					graphics.DrawString(row.Value.ToString(), textFont, brushBidText, crampedLeft, y[0], crampedAlignment);
				}
			}

			// draw ask sum
			y = GetYsByPrice(rowsA.Max(kv => kv.Key));
			graphics.DrawString(rowsAsk.Sum(v => v.Value).ToString(), textFont, brushAskTextSum, txtLeft, y[0] - halfHeight, sumAskAlignment);

			// draw bid sum
			y = GetYsByPrice(rowsB.Min(kv => kv.Key) - TickSize);
			graphics.DrawString(rowsBid.Sum(v => v.Value).ToString(), textFont, brushBidTextSum, txtLeft, y[0] - halfHeight, sumBidAlignment);

			// draw median
			int minAsk = ChartControl.GetYByValue(Bars, rowsA.Min(v => v.Key));
			int maxBid = ChartControl.GetYByValue(Bars, rowsB.Max(v => v.Key));
			int y1 = minAsk + (maxBid - minAsk) / 2;
			graphics.DrawLine(penOrderBookMedian, bounds.Left, y1, bounds.Right, y1);
		}

		private int[] GetYsByPrice(double price) {
			int barSpacing = 1; // TODO: should be configurable?

			int y0 = ChartControl.GetYByValue(this, price);
			int y1 = ChartControl.GetYByValue(this, price - TickSize);
			int height = y1 - y0 - barSpacing;

			return new int[] { y0, y1, height, Math.Max(minBarHeight, Math.Min(maxBarHeight, height)) };
		}

		protected override void OnBarUpdate() {
		}

		#endregion Methods

		#region Miscellaneous - see @BarTimer.cs

		public enum HorizontalAlignment {
			Right = 0,
			Left = 1
		}

		private T ParseEnum<T>(string value) {
			return (T)Enum.Parse(typeof(T), value, true);
		}

		private DateTime Now {
			get {
				DateTime now = (Bars.MarketData.Connection.Options.Provider == Cbi.Provider.Replay ? Bars.MarketData.Connection.Now : DateTime.Now);

				if (now.Millisecond > 0)
					now = Cbi.Globals.MinDate.AddSeconds((long)System.Math.Floor(now.Subtract(Cbi.Globals.MinDate).TotalSeconds));

				return now;
			}
		}

		private bool DisplayTime() {
			if (ChartControl != null
					&& Bars != null
					&& Bars.Count > 0
					&& Bars.MarketData != null
					&& Bars.MarketData.Connection.PriceStatus == Cbi.ConnectionStatus.Connected
					&& Bars.Session.InSession(Now, Bars.Period, true, Bars.BarsType))
				return true;

			return false;
		}

		#endregion Miscellaneous - see @BarTimer.cs

		#region Properties

		[Category("Settings")]
		[Gui.Design.DisplayName("Bar width (max.)")]
		public int MaxBarWidth {
			get { return maxBarWidth; }
			set { maxBarWidth = Math.Max(Math.Min(value, int.MaxValue), 1); }
		}

		[Category("Settings")]
		[Gui.Design.DisplayName("Bar height (max.)")]
		public int MaxBarHeight {
			get { return maxBarHeight; }
			set { maxBarHeight = Math.Max(Math.Min(value, int.MaxValue), minBarHeight); }
		}

		[Category("Settings")]
		[Gui.Design.DisplayName("Bar height (min.)")]
		public int MinBarHeight {
			get { return minBarHeight; }
			set { minBarHeight = Math.Max(Math.Min(value, maxBarHeight), 1); }
		}

		[Category("Settings")]
		[Gui.Design.DisplayName("Margin left")]
		public int MarginLeft {
			get { return marginLeft; }
			set { marginLeft = Math.Max(Math.Min(value, int.MaxValue), 0); }
		}

		[Category("Settings")]
		[Gui.Design.DisplayName("Margin right")]
		public int MarginRight {
			get { return marginRight; }
			set { marginRight = Math.Max(Math.Min(value, int.MaxValue), 0); }
		}

		[Category("Settings")]
		[Gui.Design.DisplayName("Row limit")]
		public int RowLimit {
			get { return rowLimit; }
			set { rowLimit = Math.Max(Math.Min(value, int.MaxValue), 1); }
		}

		[XmlIgnore()]
		[Description("Text font")]
		[Category("Objects")]
		[Gui.Design.DisplayName("Text font")]
		public Font TextFont {
			get { return textFont; }
			set { textFont = value; }
		}

		[Browsable(false)]
		public string TextFontSerialize {
			get { return SerializableFont.ToString(textFont); }
			set { textFont = SerializableFont.FromString(value); }
		}

		[XmlIgnore()]
		[Category("Ask colors")]
		[Gui.Design.DisplayName("Bar color")]
		public Color ColorAskBars {
			get { return colorAskBars; }
			set { colorAskBars = value; }
		}

		[Browsable(false)]
		public string ColorAskBarsSerialize {
			get { return SerializableColor.ToString(colorAskBars); }
			set { colorAskBars = SerializableColor.FromString(value); }
		}

		[XmlIgnore()]
		[Category("Ask colors")]
		[Gui.Design.DisplayName("Text color (Volume)")]
		public Color ColorAskText {
			get { return colorAskText; }
			set { colorAskText = value; }
		}

		[Browsable(false)]
		public string ColorAskTextSerialize {
			get { return SerializableColor.ToString(colorAskText); }
			set { colorAskText = SerializableColor.FromString(value); }
		}

		[XmlIgnore()]
		[Category("Ask colors")]
		[Gui.Design.DisplayName("Text color (Sum)")]
		public Color ColorAskTextSum {
			get { return colorAskTextSum; }
			set { colorAskTextSum = value; }
		}

		[Browsable(false)]
		public string ColorAskTextSumSerialize {
			get { return SerializableColor.ToString(colorAskTextSum); }
			set { colorAskTextSum = SerializableColor.FromString(value); }
		}

		[Category("Ask colors")]
		[Gui.Design.DisplayName("Bar opacity (%)")]
		public int OpacityAskBars {
			get { return opacityAskBars; }
			set { opacityAskBars = Math.Max(Math.Min(value, 100), 0); }
		}

		[XmlIgnore()]
		[Category("Bid colors")]
		[Gui.Design.DisplayName("Bar color")]
		public Color ColorBidBars {
			get { return colorBidBars; }
			set { colorBidBars = value; }
		}

		[Browsable(false)]
		public string ColorBidBarsSerialize {
			get { return SerializableColor.ToString(colorBidBars); }
			set { colorBidBars = SerializableColor.FromString(value); }
		}

		[XmlIgnore()]
		[Category("Bid colors")]
		[Gui.Design.DisplayName("Text color (Volume)")]
		public Color ColorBidText {
			get { return colorBidText; }
			set { colorBidText = value; }
		}

		[Browsable(false)]
		public string ColorBidTextSerialize {
			get { return SerializableColor.ToString(colorBidText); }
			set { colorBidText = SerializableColor.FromString(value); }
		}

		[XmlIgnore()]
		[Category("Bid colors")]
		[Gui.Design.DisplayName("Text color (Sum)")]
		public Color ColorBidTextSum {
			get { return colorBidTextSum; }
			set { colorBidTextSum = value; }
		}

		[Browsable(false)]
		public string ColorBidTextSumSerialize {
			get { return SerializableColor.ToString(colorBidTextSum); }
			set { colorBidTextSum = SerializableColor.FromString(value); }
		}

		[Category("Bid colors")]
		[Gui.Design.DisplayName("Bar opacity (%)")]
		public int OpacityBidBars {
			get { return opacityBidBars; }
			set { opacityBidBars = Math.Max(Math.Min(value, 100), 0); }
		}

		[XmlIgnore()]
		[Category("Objects")]
		[Gui.Design.DisplayName("Median line")]
		[TypeConverter(typeof(PenConverter))]
		[Editor(typeof(PenEditor), typeof(System.Drawing.Design.UITypeEditor))]
		public Pen PenOrderBookMedian {
			get { return penOrderBookMedian; }
			set { penOrderBookMedian = (value == null) ? new Pen(Color.Silver) : value; }
		}

		[Browsable(false)]
		public SerializablePen PenOrderBookMedianSerialize {
			get { return SerializablePen.FromPen(penOrderBookMedian); }
			set { penOrderBookMedian = SerializablePen.ToPen(value); }
		}

		[XmlIgnore()]
		[Category("Settings")]
		[NinjaTrader.Gui.Design.DisplayName("Alignment")]
		public HorizontalAlignment Alignment {
			get { return alignment; }
			set { alignment = value; }
		}

		[Browsable(false)]
		public string WindowBorderStyleSerialize {
			get { return alignment.ToString(); }
			set { alignment = ParseEnum<HorizontalAlignment>(value); }
		}

		#endregion Properties

	}
}
