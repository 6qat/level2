Changelog
=========

1.0.4
-----

* added row limit property
* proper default values


1.0.3
-----

* this is only a bugfix release
* histogram is always visible now (even on historical bars)
* wrong median line settings can crash indicator no more


1.0.2
-----

* added alignment and margin properties
* added order book median line
* show indicator on multi-data-series-charts
* proper vertical location


1.0.1
-----

* added font property and changed default font (Arial, 8pt)
* added bar size properties (width, height)
* rewritten cramped mode calculations
* rewritten error notification
* removed auto scale mode


1.0.0
-----

* initial release
